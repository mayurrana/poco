#include<ProducerThread.h>
#include<PocoRestClient.h>
#include<MultithreadedObserverPattern.h>

void ProducerThread::run()
{
	PocoRestClient pocoRestClient;
	Poco::JSON::Object obj = pocoRestClient.EditRequest();
	pocoRestClient.DeleteRequest();
	multithreadedObserverPattern->notifyALL(obj);
}

