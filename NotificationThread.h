#ifndef _notification_thread_
#define _notification_thread_

#include<Poco/Thread.h>
#include<Poco/Runnable.h>

class NotificationThread : public Poco::Runnable
{
		std::vector<Poco::BasicEvent<Poco::JSON::Object> *> eventVector;
		Poco::JSON::Object jsonObj;
	public:
		NotificationThread(){}
		NotificationThread(std::vector<Poco::BasicEvent<Poco::JSON::Object> *> &eventVector, Poco::JSON::Object &jsonObj)
		{
			this->eventVector = eventVector;
			this->jsonObj = jsonObj;
		}
		void run()
		{
			std::vector<Poco::BasicEvent<Poco::JSON::Object> *>::iterator iterEvent;
        		for(iterEvent = eventVector.begin(); iterEvent != eventVector.end(); iterEvent++)
        		{
                		(*iterEvent)->notify(this, jsonObj);
        		}
		}
};

#endif
