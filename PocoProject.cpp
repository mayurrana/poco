#include<ConsumerThread.h>
#include<ProducerThread.h>
#include<MultithreadedObserverPattern.h>
#include <unistd.h>
int main()
{
	//Event *jsonevent = new JsonEvent(false);
	MultithreadedObserverPattern *multithreadedObserverPattern = new MultithreadedObserverPattern(2);
	ConsumerThread *consumerThread1 = new ConsumerThread("ConsumerThread" , 1);
	ConsumerThread *consumerThread2 = new ConsumerThread("ConsumerThread" , 2);
	ConsumerThread *consumerThread3 = new ConsumerThread("ConsumerThread" , 3);
	multithreadedObserverPattern->addListner(consumerThread1);
	multithreadedObserverPattern->addListner(consumerThread2);
	multithreadedObserverPattern->addListner(consumerThread3);
	ProducerThread producerThread("ProducerThread", 1, multithreadedObserverPattern);

	Poco::Thread thread1, thread2, thread3, thread4;
	thread4.start(producerThread);
	thread1.start(*consumerThread1);
	thread2.start(*consumerThread2);
	thread3.start(*consumerThread3);

	thread4.join();
	thread1.join();
	thread2.join();
	thread3.join();

	return 0;
}
