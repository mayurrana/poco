#ifndef _consumer_thread_
#define _consumer_thread_

#include<iostream>
#include<Poco/Thread.h>
#include<Poco/Runnable.h>
#include<Poco/NotificationQueue.h>
#include<Poco/Notification.h>
#include<Poco/BasicEvent.h>
#include<Poco/JSON/Object.h>
#include<EventNotification.h>
#include<Poco/Delegate.h>

class ConsumerThread: public Poco::Runnable
{
	private:
    	std::string threadName;
    	int threadId;
		Poco::NotificationQueue queue;
	public:
		ConsumerThread(){}
    	ConsumerThread(const std::string& threadName, int threadId):
        	threadName(threadName),
        	threadId(threadId){}

    	void run();
    	void addToQueue(Poco::BasicEvent<Poco::JSON::Object> *event);
	void onEvent(const void* pSender, Poco::JSON::Object &obj)
	{
        	obj.stringify(std::cout);
	}

	

};
#endif

