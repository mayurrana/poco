#include<MultithreadedObserverPattern.h>
#include <unistd.h>
#include<NotificationThread.h>
void MultithreadedObserverPattern::addListner(ConsumerThread *consumerThread)
{
	mutex.lock();
	listConsumers.push_back(consumerThread);
	mutex.unlock();
}
void MultithreadedObserverPattern::notifyALL(Poco::JSON::Object &obj)
{
	std::vector<ConsumerThread *>::iterator iterator = listConsumers.begin();
	std::vector<Poco::BasicEvent<Poco::JSON::Object> *> eventVector[threadCount];
	while(iterator != listConsumers.end())
	{
		for(int i = 0; i < threadCount; i++)
		{
			if(iterator != listConsumers.end())
			{
				Poco::BasicEvent<Poco::JSON::Object> *event = new Poco::BasicEvent<Poco::JSON::Object>();
				(*iterator)->addToQueue(event);
				eventVector[i].push_back(event);
				iterator++;
			}
		}
	}
	sleep(10);
	Poco::ThreadPool threadPool;
	for(int i = 0; i < threadCount; i++)
        {
		NotificationThread *notifyThread = new NotificationThread(eventVector[i], obj);
		threadPool.start(*notifyThread);
	}
		threadPool.joinAll();
	/*std::vector<Poco::BasicEvent<Poco::JSON::Object> *>::iterator iterEvent;
	for(iterEvent = eventVector.begin(); iterEvent != eventVector.end(); iterEvent++)
	{
		(*iterEvent)->notify(this, obj);
	}*/
	
}

