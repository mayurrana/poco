#ifndef _event_handler_
#define _event_handler_

#include<iostream>
#include<Poco/JSON/Object.h>
/*This class is for handling the event */
class EventHandler
{
	public:
       void onEvent(const void* pSender, Poco::JSON::Object &obj)
       {
		std::cout<< "inside event handler" << std::endl;
                obj.stringify(std::cout);
       }


};
#endif
