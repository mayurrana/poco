#ifndef _event_notification_
#define _event_notification_


#include<Poco/Notification.h>
#include<Poco/BasicEvent.h>
#include<Poco/JSON/Object.h>

class EventNotification : public Poco::Notification
{
	private:
		Poco::BasicEvent<Poco::JSON::Object> *event;
	public:
		EventNotification(){}
		EventNotification(Poco::BasicEvent<Poco::JSON::Object> *event)
		{
			this->event = event;
		}
		Poco::BasicEvent<Poco::JSON::Object> * getEvent()
		{
			return event;
		}

};
#endif

