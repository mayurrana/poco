#include<iostream>
#include<Poco/Thread.h>
#include<Poco/Runnable.h>
#include<MultithreadedObserverPattern.h>

class ProducerThread: public Poco::Runnable
{
	private:
    	std::string threadName;
    	int threadId;
		MultithreadedObserverPattern *multithreadedObserverPattern;
	public:
		ProducerThread() {}
    	ProducerThread(const std::string& threadName, int threadId, MultithreadedObserverPattern *multithreadedObserverPattern):
        	threadName(threadName),
        	threadId(threadId),
			multithreadedObserverPattern(multithreadedObserverPattern){}
		void run();
};

