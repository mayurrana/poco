#include<PocoRestClient.h>

PocoRestClient::PocoRestClient()
{
	uri = "http://jsonplaceholder.typicode.com/";
	session.setHost(uri.getHost());
	session.setPort(uri.getPort());
}

void PocoRestClient::GetRequest()
{
	Poco::Net::HTTPRequest req(Poco::Net::HTTPRequest::HTTP_GET, "/posts/1" );
	req.setContentType("application/json");
	req.setKeepAlive(true);
	std::ostream& out = session.sendRequest(req) ;

	Poco::Net::HTTPResponse res;
	std::istream &is = session.receiveResponse(res);
	Poco::StreamCopier::copyStream(is, std::cout);
}

Poco::JSON::Object PocoRestClient::EditRequest()
{
	Poco::JSON::Object createObj;
	createObj.set("userId" , "1");
	createObj.set("id" , "1");
	createObj.set("title", "Mayur Rana");
	createObj.set("body", "Test Data");
	std::stringstream ss;

	Poco::Net::HTTPRequest createReq(Poco::Net::HTTPRequest::HTTP_PUT, "/posts/1");
	Poco::Net::HTTPResponse response;
	createReq.setContentType("application/json");
	createReq.setKeepAlive(true);
	createReq.setContentLength(ss.str().size());
	std::ostream& newOut = session.sendRequest(createReq);
	std::cout << response.getStatus() << " " << response.getReason() << std::endl;
	std::istream& s = session.receiveResponse(response);
	Poco::StreamCopier::copyStream(s, std::cout);
	return createObj;
}

void PocoRestClient::DeleteRequest()
{
	Poco::Net::HTTPRequest req1(Poco::Net::HTTPRequest::HTTP_DELETE, "/posts/1" );
	req1.setContentType("application/json");
	req1.setKeepAlive(true);
	std::ostream& out1 = session.sendRequest(req1) ;
	
	Poco::Net::HTTPResponse res1;
	std::istream &is1 = session.receiveResponse(res1);
	Poco::StreamCopier::copyStream(is1, std::cout);

}
