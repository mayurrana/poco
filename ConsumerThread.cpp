#include<ConsumerThread.h>
#include<EventHandler.h>

void ConsumerThread::addToQueue(Poco::BasicEvent<Poco::JSON::Object> *event)
{
	queue.enqueueNotification(new EventNotification(event));
}

void ConsumerThread::run()
{
	Poco::Notification::Ptr pNotification = queue.waitDequeueNotification();
	EventNotification *ptr = (EventNotification *) pNotification.get();
	std::cout << "Thread Id = " << threadName << " = " << threadId << " got the update notification " << std::endl;
	Poco::BasicEvent<Poco::JSON::Object> *event = ptr->getEvent();
	EventHandler eventHandler;
	*event += Poco::delegate(&eventHandler, &EventHandler::onEvent);
	std::cout << " delegate added = " << std::endl;
	//Poco::ActiveResult<Poco::JSON::Object> retArg = event->notifyAsync(0, 0);
	//retArg.wait();
//	ptr->getEvent()->stringify(std::cout);
}
