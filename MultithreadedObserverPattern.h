#ifndef _multi_thread_
#define _multi_thread_

#include<iostream>
#include<Poco/Mutex.h>
#include<Poco/Thread.h>
#include<ConsumerThread.h>
#include<vector>
class MultithreadedObserverPattern 
{
	private:
		Poco::Mutex mutex;
		std::vector<ConsumerThread *> listConsumers;
		int threadCount;
	public:
		MultithreadedObserverPattern(){}
		MultithreadedObserverPattern(const int threadCount)
		{
			this->threadCount = threadCount;
		}
		void addListner(ConsumerThread *consumerThread);
		void notifyALL(Poco::JSON::Object &obj);
	
};
#endif
