TARGET = pocoProject

CC = ${CC_HOME}/bin/gcc
CXX = ${CC_HOME}/bin/g++
INCLUDE_DIRS += -I${CC_HOME}/include/c++/4.8.3

CFLAGS = -m64 -g 
CXXFLAGS = -std=c++11

INCLUDE_DIRS += -I${POCO_HOME}/Net/include/ -I${POCO_HOME}/Foundation/include  -I${POCO_HOME}/JSON/include/ -I.

ALSO_MAKEDEPS = -MP -MMD -pthread

LIBS += -L${POCO_HOME}/lib/Linux/x86_64/ -lPocoNet \
	-L${POCO_HOME}/lib/Linux/x86_64/ -lPocoFoundation \
	-L${POCO_HOME}/lib/Linux/x86_64/ -lPocoJSON

OBJS =  PocoRestClient.o \
	ProducerThread.o \
	MultithreadedObserverPattern.o \
	ConsumerThread.o \
	PocoProject.o
       

%.o : %.cpp
	${CXX} ${INCLUDE_DIRS} ${CFLAGS} ${CXXFLAGS} ${ALSO_MAKEDEPS} -c $< -o $@

pocoProject: $(OBJS)
	${CXX} ${CFLAGS} -o $@ ${LIBS} ${OBJS}

clean:
	- rm -f ${TARGET} ${OBJS} *.d
