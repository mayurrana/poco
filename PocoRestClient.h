#include<Poco/URI.h>
#include<Poco/Net/HTTPClientSession.h>
#include<Poco/Net/HTTPRequest.h>
#include<Poco/Net/HTTPResponse.h>
#include<Poco/JSON/Object.h>
#include<Poco/StreamCopier.h>
#include<iostream>
class PocoRestClient{
	private:
		Poco::URI uri;
		Poco::Net::HTTPClientSession session;
	public:
		PocoRestClient();
		void GetRequest();	
		Poco::JSON::Object EditRequest();
		void DeleteRequest();
};
